import argparse
import fnmatch
import logging
import os
import re
import sys
from collections import defaultdict

# logging.basicConfig(level=logging.INFO,
#                     filename='check_lib_version_missing_debug.log',
#                     filemode='w',
#                     format='%(name)s - %(levelname)s - %(message)s')
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)-8s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')

# File filters to identify files for validation
REQUIREMENT_FILE_FILTER = "requirements*.txt"
GLUE_VARS_FILE_FILTER = "glue*.*vars"

ERROR_OUTPUT_FILE_NAME = 'check_python_lib_pinned_ver_errors.log'

# regex pattern for the additional-python-modules tag
# Eg. "--additional-python-modules" = "smart-open==5.2.1"'
additional_python_modules_regex = re.compile(
    r'\s*\"--additional-python-modules\"\s*=\s*\"(.*)\"')

# regex pattern for the libraries with version.
# Eg - pytest==7.2.1
library_version_regex = re.compile(r'\s*([\w.-]+\n?)(==)*[\w\.-]+')

python_lib_version_missing_errors = defaultdict(list)

error_line1 = "Library with no pinned versions are mentioned in file(s)::: "
error_line2 = "#" * 10 + " Library List with issues " + "#" * 10
error_line_end = "+" * 20 + "\n"


def check_single_library(filtered_line: str, line_number: int,
                         filtered_file: str,
                         error_dict: defaultdict(list)) -> None:
    """
    Check a given file.
    If version is not mentioned,
    update the global dictionary python_lib_version_missing_errors
    :param filtered_line: A filtered single line on which to search for
    the library version - Eg. pre-commit==2.21.0, pytest==7.2.1
    :param line_number: line number of the given lib in the file
    :type line_number: int
    :type filtered_line: string:
    :param filtered_file: A single file on which validation is performed.
    :type filtered_file: string
    :param error_dict: Dictionary to which validation errors are added.
    :type error_dict: defaultdict(list):
    """
    # split the single line by delimiter - comma (,)
    list_lib_with_potential_version = filtered_line.split(',')
    logging.debug("#####Checking library(s) - %s - START " % filtered_line)
    for lib in list_lib_with_potential_version:
        match_object = library_version_regex.match(lib)
        if match_object is not None:
            library_with_potential_version = match_object.group()
            # if second group does not exists, then version is not mentioned
            if match_object.group(2) is None:
                logging.error(
                    "Library name - %s - does not have mentioned version" %
                    library_with_potential_version)
                error_dict[filtered_file].append(
                    (library_with_potential_version.strip(), line_number))
    logging.debug("#####Checking library(s) - %s - END " % filtered_line)


def check_requirements_file(filtered_file: str,
                            error_dict: defaultdict(list)) -> None:
    """
    Check a given requirements.txt file.
    If version is not mentioned,
    update the global dictionary python_lib_version_missing_errors
    :param filtered_file: A single file on which validation is performed.
    :type filtered_file: string
    :param error_dict: Dictionary to which validation errors are added.
    :type error_dict: defaultdict(list)
    :rtype: None
    """
    with open(filtered_file) as current_file:
        logging.debug("#Checking file - %s - START" % filtered_file)
        for i, line in enumerate(current_file):
            check_single_library(
                line.strip(), i + 1, filtered_file, error_dict)
        logging.debug("#Checking file - %s - END" % filtered_file)
        logging.debug("---")


def check_glue_file(filtered_file: str,
                    error_dict: defaultdict(list)) -> None:
    """Check a given requirements.txt file.
       If version is not mentioned,
       update the global dictionary python_lib_version_missing_errors

    :param filtered_file: A single file on which validation is performed.
    :param error_dict: Dictionary to which validation errors are added.
    :rtype: None
    """
    with open(filtered_file) as current_file:
        logging.debug("#Checking file - %s - START" % filtered_file)
        for i, line in enumerate(current_file):
            match_object = additional_python_modules_regex.match(line)
            if match_object is not None:
                # group-1 has the comma-delimited libraries
                library_name = match_object.group(1)
                logging.info(
                    "Processing the line with library  %s in file %s, " %
                    (library_name, filtered_file))
                check_single_library(library_name, i + 1, filtered_file,
                                     error_dict)
        logging.debug("#Checking file - %s - END" % filtered_file)
        logging.debug("---")


def write_validation_result(validation_result_file_name: str,
                            error_dict: defaultdict(list)) -> None:
    """
    Write the errors into the filename given as argument
    :param validation_result_file_name: File name to which errors are written
    :type validation_result_file_name: string
    :param error_dict: Dictionary containing errors.
    :type error_dict: defaultdict
    :rtype: None
    """
    with open(validation_result_file_name, 'w') as result_file:
        error_line1_already_added = False
        for key, value_tup in error_dict.items():
            if not error_line1_already_added:
                formatted_error_output = [error_line1, key, error_line2]
                error_line1_already_added = True
            else:
                formatted_error_output = [key, error_line2]

            # Eg: {'.\\requirements-dev.txt': [('pytest', 5)]
            # From tuple format the result to be @line number(2)::pytest
            result = []
            for lib_name, line_number in value_tup:
                result.append(
                    "@line number(" + str(line_number) + ")::" + lib_name)

            formatted_error_output.extend(result)
            formatted_error_output.append(error_line_end)
            result_file.writelines("\n".join(formatted_error_output))
            logging.error("\n".join(formatted_error_output))


def check() -> None:
    """
    Checks all the requirements/glue.tfvars files depending on
    cmd line arguments in the directory

    '-r', '--run', help='''run argument;
                        possible values
                        1. 'glue' to run check against glue jobs
                        mentioned in glue*.tfvars
                        2. 'non-glue' to run check against
                        normal python libraries mentioned
                        in requirements.txt
                        3. all to run check against both
                        glue and normal python libraries
    choices=['glue', 'non-glue', 'all']

    '-o', '--output', help='output error file name',
                    default=check_python_libraries_errors.log

    :param: None
    """
    parser = argparse.ArgumentParser(
        description='Check whether the python libraries have pinned version')
    parser.add_argument(
        'filenames', nargs='*',
        help='Filenames pre-commit believes are changed.',
    )
    parser.add_argument('-r', '--run', help='''run argument;
                        possible values
                        1. 'glue' to run check against glue jobs
                        mentioned in glue*.tfvars
                        2. 'non-glue' to run check against
                        normal python libraries mentioned
                        in requirements.txt
                        3. all to run check against both
                        glue and normal python libraries
                        ''',
                        choices=['glue', 'non-glue', 'all'], required=True)
    parser.add_argument('-d', '--dir', help='start directory for the check',
                        default='.')
    parser.add_argument('-o', '--output', help='output error file name',
                        default=ERROR_OUTPUT_FILE_NAME)

    args = parser.parse_args()
    # logging.debug("received command line args - ", args)
    logging.debug(
        "Starting to find files matching the criteria for %s", args.run)
    for root, subdir, files in os.walk(args.dir):
        if args.run in ['non-glue', 'all']:
            try:
                # Do a directory walk and find files which are
                # python requirements.txt files.
                for file in fnmatch.filter(files, REQUIREMENT_FILE_FILTER):
                    selected_file = os.path.join(root, file)
                    check_requirements_file(
                        selected_file, python_lib_version_missing_errors)
            except Exception as e:
                logging.error(str(e), e)

        if args.run in ['glue', 'all']:
            try:
                # Do a directory walk and find files which are
                # python glue_tfvars files.
                for file in fnmatch.filter(files, GLUE_VARS_FILE_FILTER):
                    selected_file = os.path.join(root, file)
                    check_glue_file(
                        selected_file, python_lib_version_missing_errors)
            except Exception as e:
                logging.error(str(e), e)

    logging.info("Checking whether are any errors found?")
    # print all the errors in the python_lib_version_missing_errors dict
    if bool(python_lib_version_missing_errors):
        logging.error(python_lib_version_missing_errors)

        try:
            write_validation_result(
                args.output, python_lib_version_missing_errors)
            sys.exit(1)
        except Exception as e:
            logging.error(str(e), e)
    else:
        logging.info("No validation errors found in the files")


if __name__ in ['__main__']:
    check()
