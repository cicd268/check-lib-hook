import setuptools

# Package description
with open("README.md", "r") as fh:
    long_description = fh.read()

# Unable to load yaml pkg - pyyaml in new virtual environment
# that pre-commit creates in the client environment.

# Package configuration
# with open(r'config.yml') as config_file:
#     configs = yaml.load(config_file, Loader=yaml.FullLoader)

configs = {
    # Artifact name
    "artifact_name": "cdp-check-lib",

    # Artifact metadata
    "author": "Venkatesh Babu",
    "author_email": "venkatesh.babu2@cognizant.com",
    "description": "Package to check whether mentioned libs have pinned version",
    "url": "https://gitlab.bayer.com/ph-cdp/commons/libraries/cdp-pre-commit/cdp_check_lib",

    # Run unit test
    "run_test": "True",

    # Disable Sonar scanner
    "sonar": "False"
}

# Package version
version = {}
with open("version.py") as fp:
    exec(fp.read(), version)

# Installed requirements
requirements = []
with open("requirements.txt") as fp:
    requirements = fp.read().splitlines()
print(*requirements)

setuptools.setup(
    name=configs['artifact_name'],
    version=version['__version__'],
    author=configs['author'],
    author_email=configs['author_email'],
    description=configs['description'],
    long_description=long_description,
    long_description_content_type="text/markdown",
    url=configs['url'],
    packages=setuptools.find_namespace_packages(
        where='src'
    ),
    package_dir={"": "src"},
    entry_points={
        'console_scripts': [
            'check-python-lib-pinned-ver=check_lib.check_python_lib_pinned_ver:check',
        ],
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.7",
    install_requires=requirements
)
